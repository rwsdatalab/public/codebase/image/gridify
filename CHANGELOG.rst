Changelog
=========

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[0.3.1]
------------

Added
"""""

* Updated CI templates for automatic release to pypi.

[0.3.0]
------------

Added
"""""

* **overlay_gridify** method: Generate a grid after performing a spatial overlay between the primary and secondary geometry.
* **GridifyProcessor** class: Class to straightforwardly generate a grid from e.g. shapefile data.

[Unreleased]
------------

Added
"""""

* Empty Python project directory structure
