import os
import shutil

import geopandas as gpd
import pytest
import shapely.geometry


@pytest.fixture(scope="session")
def test_data_dir(tmpdir_factory):
    tmpdir_factory.mktemp("temp_test_data")
    temp_test_dir = os.path.join(tmpdir_factory.getbasetemp(), "temp_test_data")
    local_test_dir = os.path.join(os.path.dirname(__file__), "data")
    shutil.copytree(local_test_dir, temp_test_dir)
    return temp_test_dir


@pytest.fixture
def include_area():
    part1 = shapely.geometry.box(
        minx=0,
        miny=0,
        maxx=0.5,
        maxy=1,
    )
    part2 = shapely.geometry.LineString(
        [
            (0.5, 0),
            (5 / 6, 1.0),
        ]
    )
    gdf = gpd.GeoDataFrame({"col1": [1, 2]}, geometry=[part1, part2])
    return gdf


@pytest.fixture
def exclude_area():
    exclude = shapely.geometry.box(
        minx=0.5,
        miny=0.5,
        maxx=1.1,
        maxy=1.1,
    )
    gdf = gpd.GeoDataFrame({"col1": [1]}, geometry=[exclude])
    return gdf
